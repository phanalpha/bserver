import tornado.web

import handler
import settings


class Application(tornado.web.Application):
    def __init__(self):
        super(Application, self).__init__(handler.route.master.spec, **settings.tornado)
