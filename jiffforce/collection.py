def update_recursive(d, *args):
    for s in args:
        for k in s:
            if k in d and \
               type(d[k]) is dict and \
               type(s[k]) is dict:
                update_recursive(d[k], s[k])
            else:
                d[k] = s[k]

def merge_recursive(*args):
    d = dict()
    update_recursive(d, *args)

    return d
