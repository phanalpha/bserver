import os.path
from enum import Enum
from wand.image import Image
from . import fs

Sizing = Enum('Sizing', 'crop fill')

def cook(infile, outfile, width, height, sizing = Sizing.crop):
    with Image(filename=infile) as img:
        w, h = img.size
        if width == 0:
            width = w * height / h;
        if height == 0:
            height = h * width / w;

        if sizing == Sizing.crop:
            x, y = 0, 0
            if w * height < width * h:
                w, h    = width, width * h / w
                y       = (h - height) / 2
            else:
                w, h    = w * height / h, height
                x       = (w - width) / 2
            img.resize(w, h)
            img.crop(x, y, width=width, height=height)
        else:
            if width * h < w * height:
                w, h    = width, width * h / w
            else:
                w, h    = w * height / h, height
            img.resize(w, h)

        fs.ensure_dir_exists(os.path.dirname(outfile))
        img.save(filename=outfile)
