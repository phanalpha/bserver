from jiffforce import image

def cook(args):
    sizing = image.Sizing.crop if args.sizing == 'crop' else image.Sizing.fill
    image.cook(args.infile, args.outfile, args.width, args.height, sizing)


from . import command

parser  = command.add_parser('cook')
parser.set_defaults(fn=cook)
parser.add_argument('infile', type=str)
parser.add_argument('outfile', type=str)
parser.add_argument('width', type=int)
parser.add_argument('height', type=int)
parser.add_argument('-s', '--sizing', type=str, default='crop')
