from mako.template import Template
import os.path

def freeze(args):
    cd = os.path.dirname(__file__)
    with open(os.path.join(cd, '../config/rev.py'), 'wb') as f:
        f.write(Template(filename=os.path.join(cd, 'rev.py.mako')).render(rev=args.rev))


from . import command

parser  = command.add_parser('freeze')
parser.set_defaults(fn=freeze)
parser.add_argument('rev', type=str)
