import tornado.ioloop
from tornado.options import parse_command_line

from app import Application

def start(args):
    parse_command_line([])

    Application().listen(args.port, args.host)
    tornado.ioloop.IOLoop.instance().start()

from . import command

parser  = command.add_parser('start')
parser.set_defaults(fn=start)
parser.add_argument('-p', '--port', type=int, default=9000, help='listen on given port')
parser.add_argument('--host', type=str, default='127.0.0.1', help='listen on given address')
