#! /usr/bin/env python

import settings

from command import parser

if __name__ == '__main__':
    parser.add_argument('-e', '--env', type=str, default='dev', help='environment, the')
    args = parser.parse_args()
    settings.mount(args.env)
    args.fn(args)
