import tornado.web
import settings

from route import route


@route('/v')
class RevisionHandler(tornado.web.RequestHandler):
    def get(self):
        self.finish(dict(revision=settings.revision))
