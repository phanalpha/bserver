import tornado.web
import hashlib
import mimetypes
import os.path
import os

import settings

from route import route
from jiffforce import fs, image


@route('/')
class ArticleHandler(tornado.web.RequestHandler):
    def post(self):
        self.finish(dict(
            article=[
                self.persist(f)
                for f in self.request.files.get('article', [])
            ]
        ))

    def persist(self, fi):
        localname = hashlib.sha1(fi['body']).hexdigest() + \
                    os.path.splitext(fi['filename'])[-1]
        path = os.path.join(
            settings.tornado['static_path'],
            localname[0:2],
            localname[2:4],
            localname
        )

        if not os.path.exists(path):
            fs.ensure_dir_exists(os.path.dirname(path))
            with open(path, 'wb') as fo:
                fo.write(fi['body'])

        return localname


@route(r"/(crop|fill)/(\d+),(\d+)/\w{2}/\w{2}/((?:\w|\.)+)")
class ArticleHandler(tornado.web.RequestHandler):
    def get(self, sizing, width, height, localname):
        infile = os.path.join(
            settings.tornado['static_path'],
            localname[0:2],
            localname[2:4],
            localname
        )
        outfile = os.path.join(
            settings.tornado['static_path'],
            sizing,
            ','.join([width, height]),
            localname[0:2],
            localname[2:4],
            localname
        )
        sizing = image.Sizing.crop if sizing == 'crop' else image.Sizing.fill

        if not os.path.exists(outfile):
            image.cook(infile, outfile, int(width), int(height), sizing)

        self.serve(outfile)

    def serve(self, path):
        self.set_header('Content-Type', mimetypes.types_map[os.path.splitext(path)[-1]])
        with open(path, 'rb') as f:
            self.write(f.read())
