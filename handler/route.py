import tornado.web


class Route:

    def __init__(self, prefix=''):
        self.prefix = prefix
        self.spec   = []

    def map(self, pattern, kwargs=None, name=None):
        def decorate(handler):
            self.spec.append(tornado.web.URLSpec(self.prefix + pattern, handler, kwargs, name))

            return handler

        return decorate


master  = Route()
route   = master.map
