import os.path

from jiffforce.collection import update_recursive

tornado = dict(
    debug       = True,
    gzip        = True,
    static_path = os.path.join(os.path.dirname(__file__), 'static')
)

revision = '(dev)'


def mount(name):
    import hashlib
    import imp
    import types

    if not name.endswith('.py'):
        mount(os.path.join(os.path.dirname(__file__), 'config', name + '.py'))

    if os.path.exists(name):
        m = imp.load_source(hashlib.md5(os.path.realpath(name)).hexdigest(), name)
        update_recursive(globals(), dict(
            [(k, m.__dict__[k]) for k in dir(m)
             if not (k.startswith('_') or type(m.__dict__[k]) is types.ModuleType)]
        ))

mount('rev')
